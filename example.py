#!/usr/bin/env python2
from matplotlib import rcParams,use
#uncomment next line for saving to pdf
#use('Pdf')
use('Svg')

rcParams['font.family']='serif'
rcParams['font.size']=20
rcParams['text.usetex']=False
rcParams['text.latex.preamble']='\usepackage{fourier}'

import matplotlib.pyplot as plt
import iconicdiagramplotlib as ic

fig = ic.iconic_diagram_figure()
ax = ic.get_ic_axes(fig)


ic.fixed_world(ax, [2, 13])
ic.source_of_torque(ax, [8, 13])
ic.mass(ax, [17, 13])
ic.connector(ax,[[17,13],[23,13]])

ic.lever_less(ax,[23,13])
ic.connector(ax,[[23,9],[35,9]])
ic.spring(ax, [29, 9])
ic.lever_more(ax,[35,9])

ic.connector(ax,[[35,13],[47,13]])
ic.mass(ax, [41, 13])


startpoint = [19,27]
midpoint1 = [27,27]
midpoint2 = [27,29]
midpoint3 = [41,29]
midpoint4 = [41,27]
endpoint = [47,27]
route = [startpoint, midpoint1, midpoint2, midpoint3, midpoint4, endpoint]
ic.connector(ax,route)
ic.fixed_world(ax, startpoint)
ic.source_of_displacement(ax, [23, 27])
ic.spring(ax, [32.5, 29])
ic.connector(ax,[[27,27], [27,25], [41,25], [41,27]])
ic.damper(ax, [32.5, 25])
ic.mass(ax, [41, 27])

ic.motion_summer(ax,[49,20],14)

ic.connector(ax,[[51,27],[69,27]])
ic.spring(ax, [57, 27])
ic.mass(ax, [66, 27])
ic.displacement_bottom(ax, [66, 27], r'$x$')
ic.source_of_force(ax, [75, 27])
ic.fixed_world_r(ax, [81, 27])











#plt.show()
plt.savefig('example')
